https://www.unixmen.com/ttystudio-record-terminal-session-gif/

To install NodeJS in DEB based systems, run:

sudo apt-get install nodejs npm

In RPM based systems:

sudo yum install epel-release

sudo yum install nodejs npm

Then, install ttystudio using command:

sudo npm -g install ttystudio
