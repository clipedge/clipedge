Ansible Examples
----------------

This repository contains examples and best practices for building Ansible Playbooks.

ansible -m setup 192.168.0.8 -k

# Run first-time as root
ansible-playbook -i staging -k  site.yml --ask-vault-pass

# Subsequent runs are non-root
ansible-playbook -i staging site.yml --ask-vault-pass --ask-become-pass


https://www.pluralsight.com/blog/it-ops/linux-hardening-secure-server-checklist


https://download.opensuse.org/ports/armv7hl/factory/repo/oss/armv7hl/

https://download.opensuse.org/ports/armv7hl/factory/repo/oss/armv7hl/fuse-overlayfs-1.0.0-1.2.armv7hl.rpm
