(require 'ox-publish)
(setq org-publish-project-alist
     '(

("x-inherit"
 :base-directory "/home/nonroot/org-mode/publish_html/org/"
 :recursive t
 :base-extension "css/|js"
 :publishing-directory  "/home/nonroot/org-mode/publish_html/public_html/"
 :publishing-function org-publish-attachment
 )

("x-source"            ;org-source files to be transformed into html files
:base-directory "/home/nonroot/org-mode/publish_html/org/"
:base-extension "org"
:publishing-directory "/home/nonroot/org-mode/publish_html/public_html/"
         :recursive t
         :publishing-function org-html-publish-to-html
         :headline-levels 2               ; Just the default for this project.
         :auto-sitemap t                  ; Generate sitemap.org automagically...
         :sitemap-filename "sitemap.org"  ; ... call it sitemap.org (it's the default)...
         :sitemap-title "fyPi"         ; ... with title 'Sitemap'.
         :with-creator t    ; Disable the inclusion of "Created by Org" in the postamble.
         :with-email t      ; Disable the inclusion of "(your email)" in the postamble.
         :with-author t       ; Enable the inclusion of "Author: Your Name" in the postamble.
         :auto-preamble t         ; Enable auto preamble 
         :auto-postamble t         ; Enable auto postamble 
         :table-of-contents t        ; Set this to "t" if you want a table of contents, set to "nil" disables TOC.
         :toc-levels 1               ; Just the default for this project.
         :section-numbers t        ; Set this to "t" if you want headings to have numbers.
         :html-head-include-default-style t ;Enable the default css style
         :html-head-include-scripts t ;Disable the default javascript snippet
         :html-head "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n<link rel=\"stylesheet\" type=\"text/css\" href=\"css/org.css\"/>\n<script type=\"text/javascript\" src=\"js/ga.min.js\"></script>" ;Enable custom css style and other tags
         :html-link-home "sitemap.html"    ; Just the default for this project.
         :html-link-up "sitemap.html"    ; Just the default for this project.
)

("x-static"
:base-directory "/home/nonroot/org-mode/publish_html/org/"
:base-extension "png/css/|js/|png/|jpg/|gif/|pdf/|mp3/|ogg/|swf"
:publishing-directory "/home/nonroot/org-mode/publish_html/public_html/org/"
:recursive t
:publishing-function org-publish-attachment
)

("x-static-img"
:base-directory "/home/nonroot/org-mode/publish_html/org/img/"
:base-extension "png"
:publishing-directory "/home/nonroot/org-mode/publish_html/public_html/img/"
:recursive t
:publishing-function org-publish-attachment
)

("x-static-css"
:base-directory "/home/nonroot/org-mode/publish_html/org/css/"
:base-extension "css"
:publishing-directory "/home/nonroot/org-mode/publish_html/public_html/css/"
:recursive t
:publishing-function org-publish-attachment
)

("myproject" :components ("x-inherit" "x-source" "x-static" "x-static-img" "x-static-css"))
))

