- name: Create non-root user
  user:
    name: nonroot
    password: "{{password}}"
    comment: nonroot
    uid: 1000
    groups: wheel
    append: yes

- name: Set authorized key, removing all the authorized keys already set
  authorized_key:
    user: nonroot
    key: '{{ item }}'
    state: present
    exclusive: True
  with_file:
    - roles/common/files/id_rsa.pub

- name: Create org-mode directories if it does not exist
  file:
    path: "{{ item.path }}"
    state: directory
    mode: '0755'
    owner: nonroot
    group: nonroot
  with_items:
    - { path: '/home/nonroot/org-mode/publish_html/org/css', mode: '0755'}
    - { path: '/home/nonroot/org-mode/publish_html/public_html/css', mode: '0755'}
    - { path: '/home/nonroot/org-mode/publish_html/org/img', mode: '0755'}
    - { path: '/home/nonroot/org-mode/publish_html/public_html/img', mode: '0755'}

- name: Copy multiple files in Ansible with different permissions
  copy:
    src: "{{ item.src }}"
    dest: "{{ item.dest }}"
    mode: "{{item.mode}}"
    owner: nonroot
    group: nonroot
    backup: yes
  with_items:
    - { src: 'roles/users/files/emacs.txt',dest: '/home/nonroot/.emacs.d/init.el', mode: '0664'}
    - { src: 'roles/users/files/init.el',dest: '/home/nonroot/.emacs.d/init.el', mode: '0664'}
    - { src: 'roles/users/files/org-publish-project-alist.el',dest: '/home/nonroot/org-publish-project-alist.el', mode: '0644'}
    - { src: 'roles/users/files/one.org',dest: '/home/nonroot/org-mode/publish_html/org/one/one.org', mode: '0644'}
    - { src: 'roles/users/files/two.org',dest: '/home/nonroot/org-mode/publish_html/org/two/two.org', mode: '0644'}

# https://github.com/bbatsov/prelude/issues/1225
- name: add temporary fix for armv7hl emacs bug
  lineinfile:
    path: /home/nonroot/.emacs.d/init.el
    insertbefore: BOF
    line: "(setq gnutls-algorithm-priority \"NORMAL:-VERS-TLS1.3\")"
  when:
    - ansible_facts['distribution'] == "CentOS"
    - ansible_facts['distribution_major_version'] == "8.1"
    - ansible_facts['kernel'] == "4.19.72-300.el8.armv7hl"

# https://emacs.stackexchange.com/questions/44534/org-mode-sitemap-not-updated-after-re-publish
- name: Clean .org-timestamps path
  file:
    state: absent
    path: /home/nonroot/.org-timestamps/

# [nonroot@localhost ~]$ emacs -batch --load /home/nonroot/org-publish-project-alist.el --eval '(org-publish-project "myproject")'
# Loading /home/nonroot/.org-timestamps/x-inherit.cache...
# Resetting org-publish-cache
# Loading /home/nonroot/.org-timestamps/x-source.cache...
# Generating sitemap for fyPi
# Publishing file /home/nonroot/org-mode/publish_html/org/two/two.org using ‘org-html-publish-to-html’
# Skipping unmodified file /home/nonroot/org-mode/publish_html/org/two/index.org
# Skipping unmodified file /home/nonroot/org-mode/publish_html/org/two/foo.org
# Skipping unmodified file /home/nonroot/org-mode/publish_html/org/one/index.org
# Publishing file /home/nonroot/org-mode/publish_html/org/one/one.org using ‘org-html-publish-to-html’
# Publishing file /home/nonroot/org-mode/publish_html/org/sitemap.org using ‘org-html-publish-to-html’
# Skipping unmodified file /home/nonroot/org-mode/publish_html/org/two.org
# Skipping unmodified file /home/nonroot/org-mode/publish_html/org/one.org
# Resetting org-publish-cache
# Loading /home/nonroot/.org-timestamps/x-static.cache...
# Resetting org-publish-cache
# Loading /home/nonroot/.org-timestamps/x-static-img.cache...
# Resetting org-publish-cache
# Loading /home/nonroot/.org-timestamps/x-static-css.cache...
# [nonroot@localhost ~]$ 
- name: build html using executing emacs cli batch
  command: emacs -batch --load /home/nonroot/org-publish-project-alist.el --eval '(org-publish-project "myproject")'
