// buffover.c

#include <stdio.h>

int foo() {
  char ch;
  char buffer[5];
  int i = 0;
  printf("Say something: ");

  while ((ch = getchar()) != '\n') {
    buffer[i++] = ch;
  }

  buffer[i] = '\0';
  printf("You said: %s\n", buffer);
  return 0;
}

int main() {
  foo();
}
